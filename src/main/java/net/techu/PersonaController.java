package net.techu;

import net.techu.data.Persona;
import net.techu.data.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PersonaController {

    @Autowired
    private PersonaRepository repository;


    @GetMapping(value = "/persona", produces = "application/json")
    public ResponseEntity<List<Persona>> obtenerPersonas(){
        List<Persona> lista = repository.findAll();
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    @PostMapping(value = "/persona")
    public ResponseEntity<String> addpersona(@RequestBody Persona persona){
        Persona resultado = repository.insert(persona);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }


}
