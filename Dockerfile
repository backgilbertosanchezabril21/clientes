FROM openjdk:15-jdk-alpine
COPY target/personas-0.0.1.jar app.jar
EXPOSE 8083
ENTRYPOINT ["java", "-jar", "/app.jar"]