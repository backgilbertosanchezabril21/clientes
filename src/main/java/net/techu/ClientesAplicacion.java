package net.techu;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ClientesAplicacion {
    public static void main (String[] args){
        SpringApplication.run(ClientesAplicacion.class, args);
    }

}
